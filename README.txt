This module allows administrators to update their search indexes interactively via Batch API instead of waiting for cron. The URL admin/settings/search/reindex is the main page for the process.
